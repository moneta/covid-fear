
### Motivation
In a series of laboratory studies investigating the role of trait anxiety in aversive learning we found that contingency changes were better tracked by high trait anxiety group. Behavioral and physiological (SCR) data revealed that high trait anxious individuals had higher awareness of the current hidden state of the environment, especially in the later stages of the experiment (Fig 1). This was indexed by more distinct expectancy ratings and anticipatory SCR responses between periods of objectively low and high threat. Furthermore, we found that following changes in underlying shock contingnecy, high trait anxious individuals were faster at adjusting their expectation to the new reinforcement level, indexed by switch steepness. Computational modeling revealed that there are generally two groups of participants: those who update their expectations gradually in a trial-by-trial manner, and those that learn that there are two contingency states and switch between them in abrupt manner. Trait anxiety was associated with tendency to employ the state switching, rather than gradual learning, strategy.

*Fig. 1: Data show trial-by-trial adjustment of shock expectancy after a switch from high to low (blue) and low to high (red) probability of shock. Lighter colors show high trait anxious group.*
![behavioural_data](behavioural_data.png)

The above findings may potentially explain why we see higher rates of fear relapse in highly anxious individuals. Our group is currently conducting a study investigating whether state switching and high trait anxiety also lead to higher rates of relapse on a session one week later.

The ongoing global crisis related to the COVID-19 pandemic resembles our experimental design in number of ways: There is an initial fear period which will be followed by a phase of fear extinction and most likely (see below) by a period of fear reinstatement. We therefore see the situation as a unique opportunity to investigate fear learning in a natural setting.

### Hypotheses

*Fig. 2: Simulated time-course of threat probability estimates for high and low trait anxiety groups. Blue rectangles highlight periods where theoretical predictions for state-switchers and gradual learners diverge.*    
![fear](fear.png)

In Fig. 2 we show the main predictions based on our lab results. The predictions concern specifically threat-related probability judgments, not subjective fear.

#### Core
1. High TA participants will recognize a period of safety faster and adjust their *objective* aversive event expectations sooner.
2. High TA will be associated with faster threat probability (based on our lab results) and fear (based on literature) reinstatement  once signs of the second wave of the pandemic occur.

#### Exploratory
3. There will be a dissociation between affective and objective assessment of the situation that will be particularly pronounced in highly anxious individuals.


### Design
We propose a longitudinal study lasting 12 months in which an initial sample of 1000 native English speakers in the UK and the US will complete 12 on-line sessions during which we will collect questionnaire responses assessing personality traits (trait anxiety, catastrophizing, etc.) current level of anxiety, stress, covid-related fears and event probability estimates. Additional smaller sample of ~100 people which will recruited in person will be added to minimize risk of complete dropout.

#### Materials
##### Demographic measures (DMs)
- age
- gender
- sort code / zip code of residence (will ask again one year later)

##### Personality measures (PMs)
1. STAI-TRAIT and STICSA (STAI-TRAIT alternative)
2. BDI-II
3. General Catastrophizing (new questionnaire by Pike et al.)
4. Risk perception/attituted

##### Current state measures (CSMs)
1. STAI-STATE
2. Stress (VAS scale)
3. Factual-COVID
4. Affective-COVID
5. Probability estimates


#### Session types
**1. First Session**
 - DMs, PMs and CSMs
 - 25 min

**2. Full session**
  - PMs and CSMs
  - 20 min

**3. Check-in session**
   - only  DMs and CSMs
   - 10 min

#### Timing
There is a plan to conduct 12 sessions in total, 4 of which will be first/full sessions while the other 8 will be the shorter check-in sessions. The temporal sampling will be uneven, reflecting the expected progress of the global pandemic. This is to ensure that the critical periods (e.g. resurgence in number of cases expected in Nov/Dec 2020) are sampled with higher frequency. Crucially, the increased sampling frequency will start as soon as the news start reporting COVID19 comeback.    

*Fig. 3: Predicted health care demands, UK, next 12 months (based on Anderson et al. 2020, The Lancet)*  
![Sampling](sampling.png)
#### Payment structure
Participants will be paid at rate $9/hr (estimate from Prolific, mTurk can be cheaper). On sessions 3, 6 and 9 they will be paid $2 interim bonus and on the last session $8 overall bonus. A participant that completes all 12 sessions will earn $32. If every participant completed the study, the cost would be $32 000. However, an attrition rate is expected. Approximately 20% of participants are expected to complete the study (attrition shown on figure below), so the estimated cost for on-line sample is $13 500. Adding 100 in-person recruited participants extends the cost by further $3200, bringing the total to $16 700.


*Fig. 4: Payment structure of the experiment*  
![Payment](payment.png)

As alternative, to motivate people a bit more to complete, we can raise interim bonus to $5 and the final bonus to $10. This is something to discuss. The cost in that case would be $23 000. (but nearly $50k if they all completed the the 12 sessions).


#### Questions
This section contains all questionnaires newly designed for this study.

---

##### 1. Factual COVID-related information [Factual COVID]
**Purpose:** To assess objective individual circumstances of the respondent.  


A. *Please tick what applies to you in relation to the coronavirus:*  
- [ ] I got infected.  
- [ ] I lost my job, ran out of business or suffered dramatic economical hardship due to the virus.
- [ ] A person close to me fell seriously ill or died.


B. *Please indicate to what degree do the following apply to you __as a consequence__ of the coronavirus:*   

[Responses:
Does not apply 0:10 Strongly applies]
1. *I suffered economic impact (e.g. losing a job or running out of business)*  
2. *My mental health deteriorated as a consequence.*
3. *Since March 2020, I work from home.*
4. *Since March 2020, I have had to commute to work using public transport.*
5. *I currently commute to work using public transport.*
6. *Since March 2020, I worked in a risk profession where there was objectively higher danger of getting infected (medical, transport and other services).*
7. *I currently work in a risk profession where there was objectively higher danger of getting infected (medical, transport and other services).*
8. *I live alone.*


---

##### 2. COVID-specific fears and attitudes [Affective COVID]
**Purpose:** To assess specific fears and worries and general attitudes towards the pandemic.  

[Response:  
**Option 1:** Strongly disagree | Disagree | Agree | Strongly agree  
**Option 2:** Strongly disagree 0 1 2 3 4 5 6 7 Strongly agree
]

A. *Due to the coronavirus I am __currently__:*
1. *worried that I will get infected.*
2. *worried that I will suffer serious medical issues or die.*
3. *worried about the economic impact on me (running out of business, losing a job).*
4. *scared that something bad will happen to me or to a close person.*
5. *worried that if something happens to me there won't be adequate medical help.*
6. *worried that a close person will get infected.*
7. *worried that a close person will suffer serious medical issues or die.*

B. *Right now, we are in a period of relative danger.*
C. *I was surprised when the coronavirus became pandemic in my country.*  
D. *When the pandemic broke out, I was very scared.*  
E. *Many people are over-reacting.*  
F. *The virus is not as dangerous as it is often portrayed.*  
G. *The virus was made in a lab.*
H. *Right now, we are in a period of relative safety.*


---

##### 3. Probability estimates
**Purpose:** To assess depersonalized predictions related to the virus.

A. *Please try to objectively estimate the probability of the following events happening:*  
[Response: VAS scale 0 - 100% (increments of 10%)]
1. *You will get infected with the virus.*
2. *You will die because of the virus.*
3. *Somebody you know will get infected by the virus.*
4. *Somebody you know will die because of the virus.*
5. *You will directly suffer due to the economic impact (for example run out of business, lose a job or investment).*  
6. *The economy of your country will suffer badly and it will take years to come back to pre-2020 level.*
7. *A single average person will get infected.*  


B. *Please indicate when do you think the following will happen (or have happened):*   
*The end of the pandemic.*  [month, year]  
*Everyday life comes back to normal.*  [month, year]  
*Do you think the pandemic will comeback in a second wave? If so, when?* [yes/no]  [month, year]   
*The economy/health care will come back to normal*  [month, year]  

### Outstanding decisions and issues
1. Decide on technical delivery - so far I am expecting to use Lime Survey and Prolific.
2. Size of bonuses.
3. Finalize questions.
4. Exact sampling frequency.
5. Decide how to best monitor the 'objective' news. (Scraping google news/bbc/cnn daily?)
